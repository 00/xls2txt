use calamine::{open_workbook_auto, Reader};
use std::env;

fn main() {
    let path = env::args()
        .nth(1)
        .expect("Please provide an excel file to convert");
    
    // opens a new workbook
    let mut workbook = open_workbook_auto(path).expect("Cannot open file");

    let sheets = workbook.sheet_names().to_owned();
    for s in sheets {
        println!("sheet name: {}", s);
        let range = workbook.worksheet_range(&s).unwrap().unwrap();
        for r in range.rows() {
            for c in r.iter() {
                println!("{}", c);
            }
        }
    }
}
